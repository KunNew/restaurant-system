<?php

namespace App\Http\Controllers\Auth;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function user()
    {
        // $menus = Menu::whereNull('parent_id')->with('children', function($q){ return $q->orderByRaw('ISNULL(ordering), ordering asc'); })->orderByRaw('ISNULL(ordering), ordering asc')->get();
        $user = auth()->user();
        $permissions = $user->role->permissions->pluck('name');

        // $user->menus = $menus;
        // $user->ability = [
        //     [
        //         'action' => 'manage',
        //         'subject' => 'all',
        //     ],
        // ];
        return json_data([
            'id' => $user->id,
            'avatar' => $user->avatar,
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'role' => [
                'id' => $user->role->id,
                'name' => $user->role->name
            ],
            'permissions' => $permissions,
            'username' => $user->username,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at
        ]);
    }
    public function changeProfile(Request $request, $id)
    {
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->avatar = $request->avatar;
        if ($user->save()) return success();
        return server_error();
    }
    public function changePassword(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $user->password = Hash::make($request->password);
        if ($user->save()) return success();
        return server_error();
    }
}
