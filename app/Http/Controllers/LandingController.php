<?php

namespace App\Http\Controllers;

use App\Http\Resources\landing\CategoryResource;
use App\Http\Resources\landing\MenuResource;
use App\Http\Resources\landing\TableResource;
use App\Models\Category;
use App\Models\Menu;
use App\Models\Table;
use Illuminate\Http\Request;

class LandingController extends Controller
{


    public function getCategories()
    {
        $categories = Category::all();

        return CategoryResource::collection($categories);
    }

    public function getTables()
    {

        $tables = Table::where('status', '=', 'available')->get();
        return TableResource::collection($tables);
    }

    public function getMenus(Request $request)
    {

        $fromPrice = number_format($request->fromPrice, 2);
        $toPrice = number_format($request->toPrice, 2);

        return MenuResource::collection(Menu::with('category')->when(function ($q) {

            return $q->orWhere('name', 'LIKE', '%' . request('searchQuery') . '%');
        })->get());
        // $price = $request->price[1];
        // return json_data([
        //     'fromPrice' => $fromPrice,
        //     'toPrice' => $toPrice
        // ]);
    }
    public function getFilterMenus(Request $request)
    {



        return MenuResource::collection(Menu::with('category')->where('category_id', $request->category_id)
            ->when(function ($q) {
                return $q->where('name', 'LIKE', '%' . request('searchQuery') . '%');
            })->get());
    }


    public function getMenu($id)
    {

        $menu = Menu::find($id);
        return  new MenuResource($menu);
    }
}
