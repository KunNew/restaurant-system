<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMenuRequest;
use App\Http\Requests\UpdateMenuRequest;
use App\Http\Resources\MenuCollection;
use App\Models\Category;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        Gate::authorize('view', 'menus');

        $menus = Menu::where([
            ['name', '!=', Null],
            [function ($query) use ($request) {
                if (($s = $request->search)) {
                    $query->orWhere('name', 'LIKE', '%' . $s . '%')->get();
                    $query->orWhere('description', 'LIKE', '%' . $s . '%')->get();
                }
            }]
        ])->paginate(request('perpage', 2));

        return new MenuCollection($menus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMenuRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenuRequest $request)
    {
        Gate::authorize('create', 'menus');

        $menu = new Menu();
        $menu->avatar = $request->avatar;
        $menu->name = $request->name;
        $menu->price = $request->price;
        $menu->cost = $request->cost;
        $menu->unit = $request->unit;
        $menu->category_id = $request->category_id;
        $menu->status = $request->status;
        $menu->description = $request->description;
        if ($menu->save()) {
            // $user->sendEmailVerificationNotification();
            return success();
        }

        return server_error();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        $menu = $menu::with('category')->where('id', $menu->id)->first();
        return json_data($menu);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $this->authorize('edit', 'menus');

        return json_data([
            'data' => [
                'name' => $menu->name,
                'category_id' => $menu->category_id,
                'cost' => $menu->cost,
                'price' => $menu->price,
                'unit' => $menu->unit,
                'status' => $menu->status,
                'description' => $menu->description,
            ],
            'additional' => [
                'avatar' => $menu->avatar,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMenuRequest  $request
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenuRequest $request, Menu $menu)
    {
        $this->authorize('edit', 'menus');

        $menu->avatar = $request->avatar;
        $menu->name = $request->name;
        $menu->price = $request->price;
        $menu->cost = $request->cost;
        $menu->unit = $request->unit;
        $menu->category_id = $request->category_id;
        $menu->status = $request->status;
        $menu->description = $request->description;
        if ($menu->save()) {

            return success();
        }

        return server_error();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $this->authorize('delete', 'menus');

        if ($menu->delete()) return success();
        return server_error();
    }

    public function menu()
    {
        $menus = Menu::where('status',1)->get();
        return json_data($menus);
    }

}
