<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    public function getMyOrders()
    {

        $orders = Order::with('user')->with('table')->where("order_status", "unpaid")->get();

        return json_data($orders);
    }

    public function cancelOrder($id)
    {
        $oldOrder = Order::with('orderDetail')->find($id);
        $order = Order::find($id);
        if ($order->delete()) {
            foreach ($oldOrder->orderDetail as $detail) {
                $menu = Menu::where("id", $detail['menu_id'])->where('status', 1)->first();
                if ($menu) {
                    $menu->qty += $detail['quantity'];
                    $menu->save();
                }
                OrderDetail::where('order_id', $id)->delete();
            }
            $table = Table::find($oldOrder->table_id);
            $table->status = "available";
            $table->save();
            return success();
        } else {
            return server_error();
        }

        // return json_data();
    }
    public function confirmOrder(Request $request)
    {
        $order = new Order();
        $order->total_price = $request->total;
        $order->order_date = date('Y-m-d');
        $order->table_id = $request->table_id;
        $order->user_id = $request->user_id;
        if ($order->save()) {

            $table = Table::find($order->table_id);
            $table->status = "unavilable";
            $table->save();

            foreach ($request->detail as $detail) {
                Menu::where('id', $detail['id'])->where('status', 1)->update(['qty' => DB::raw('qty -' . $detail['qty'])]);
                $orderDetail = new OrderDetail();
                $orderDetail->order_id = $order->id;
                $orderDetail->menu_id = $detail['id'];
                $orderDetail->menu_name = $detail['name'];
                $orderDetail->menu_price = number_format($detail['price']);
                $orderDetail->quantity = $detail['qty'];
                $orderDetail->status = $detail['status'];
                $orderDetail->unit = $detail['unit'];
                $orderDetail->amount = $orderDetail->menu_price * $orderDetail->quantity;
                $orderDetail->save();
            }
            return success();
        } else {
            return server_error();
        }
    }

    public function getMenuByTableId($id)
    {

        $order = Order::with(['orderDetail' => function ($q) {
            return $q->with('menu');
        }])->with('table')->where('table_id', $id)->where('order_status', 'unpaid')->first();

        return json_data($order);
    }

    public function payOrder(Request $request)
    {



        $order = Order::find($request->id);
        $order->total_recieved = $request->total_recieved;
        $order->change = $request->change;
        $order->description = $request->description;
        $order->order_status = 'paid';
        $order->payment_type = $request->payment_type;
        if ($order->save()) {
            // foreach ($request->order_detail as $detail) {

            //     // Menu::where('id', $detail['menu_id'])->where('status', 1)->update(['qty' => DB::raw('qty -' . $detail['quantity'])]);

            //     // $menu = Menu::where('id',$detail['id'])->where('status',$detail['status'])->first();

            //     // $menu->qty = $menu->qty - $detail['quantity'];
            //     // $menu->save();
            // }
            $table = Table::find($order->table_id);
            $table->status = "available";
            $table->save();
            return success();
        } else {
            return server_error();
        }

        // return json_data($request->all());
    }
}
