<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index(Request $request)
    {
        $permissions = Permission::where([
            ['name', '!=', Null],
            [function ($query) use ($request) {
                if (($s = $request->searchQuery)) {
                   return $query->orWhere('name', 'LIKE', '%' . $s . '%');

                }
            }]
        ])->get();

        return json_data($permissions);
    }
}
