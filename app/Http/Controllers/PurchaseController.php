<?php

namespace App\Http\Controllers;

use App\Http\Resources\PurchaseCollection;
use App\Models\Menu;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PurchaseController extends Controller
{


    public function index(Request $request)
    {

        Gate::authorize('view', 'purchases');
        // $purchase = Purchase::with('supplier')->where([
        //     ['date', '!=', Null],
        //     [function ($query) use ($request) {
        //         if (($s = $request->search)) {
        //             $query->orWhere('date', 'LIKE', '%' . $s . '%')->get();
        //             // $query->orWhere('name', 'LIKE', '%' . $s . '%')->get();
        //             // $query->orWhere('description', 'LIKE', '%' . $s . '%')->get();
        //         }
        //     }]
        // ])->paginate(request('perpage', 2));
        $purchase = Purchase::with('supplier')->whereHas('supplier', function ($q) {
            return $q->where('name', 'LIKE', '%' . request('search') . '%');
        })->when('date', function ($query) {
            return $query->orWhere('date', 'LIKE', '%' . request('search') . '%');
        })->paginate(request('perpage', 2));

        // $purchase = Purchase::when($request->search, function ($q) {
        //     return $q->orWhere('date', 'LIKE', '%' . request('search') . '%');
        // })->with(['supplier' => function ($q) {
        //     // $q->orWhere('name', 'LIKE', '%' . request('search') . '%');
        //     $q->when(request('search'),function($query) {
        //         return $query->orWhere('name', 'LIKE', '%' . request('search') . '%');
        //     });
        // }])->paginate(request('perpage', 2));

        return new PurchaseCollection($purchase);
    }
    public function addPurchase(Request $request)
    {
        Gate::authorize('create', 'purchases');
        $items = $request->items;
        $purchase = new Purchase();
        $purchase->date = $request->date;
        $purchase->user_id = auth()->user()->id;
        $purchase->supplier_id = $request->supplier_id;
        $purchase->total = $request->total;
        if ($purchase->save()) {
            foreach ($items as $key => $item) {

                $purchaseDetail = new PurchaseDetail();
                $purchaseDetail->date = $purchase->date;
                $purchaseDetail->purchase_id = $purchase->id;
                $purchaseDetail->menu_id = $item['id'];
                $purchaseDetail->menu_name = $item['name'];
                $purchaseDetail->price = $item['price'];
                $purchaseDetail->cost = $item['cost'];
                $purchaseDetail->qty = $item['qty'];
                $purchaseDetail->amount = $item['qty'] * $item['price'];

                if ($purchaseDetail->save()) {
                    $menu = Menu::find($purchaseDetail->menu_id);
                    $menu->qty += $purchaseDetail->qty;
                    $menu->save();
                }
            }


            return success();
        } else {
            return server_error();
        }
    }
}
