<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Purchase;
use App\Models\Salary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class ReportController extends Controller
{
    public function purchaseReport(Request $request)
    {

        Gate::authorize('view', 'reports');
        $purchase = DB::table('purchases')->join('suppliers', 'purchases.supplier_id', 'suppliers.id')->select('purchases.*', 'suppliers.name')->whereBetween('date', [$request->fromDate, $request->toDate])->when($request->supplier_id, function ($q) {
            return $q->where('purchases.supplier_id', request('supplier_id'));
        })->get();

        // orWhere('purchases.supplier_id', $request->supplier_id)->



        return json_data($purchase);
    }

    public function purchaseDetailBySupplier(Request $request)
    {

        Gate::authorize('view', 'reports');
        // $purchase = DB::table('purchases')->join('suppliers', 'purchases.supplier_id', 'suppliers.id')->select('purchases.*', 'suppliers.name')->groupBy('purchases.user_id')->whereBetween('date', [$request->fromDate, $request->toDate])->when($request->supplier_id, function ($q) {
        //     return $q->where('purchases.supplier_id', request('supplier_id'));
        // })->get();
        //  $purchase = DB::table('purchases')->join('suppliers', 'purchases.supplier_id', 'suppliers.id')->select('purchases.*', 'suppliers.name')->get();

        // orWhere('purchases.supplier_id', $request->supplier_id)->


        $purchases = Purchase::with('purchaseDetails')->with('supplier')->whereBetween('date', [$request->fromDate, $request->toDate])->when($request->supplier_id, function ($q) {
            return $q->where('supplier_id', request('supplier_id'));
        })->get();

        // $purchases = DB::table('purchases')->join('purchase_details', 'purchase_details.purchase_id', 'purchases.id')->select('purchase_details.*', 'purchases.*')->groupBy('purchase_details.purchase_id')->get();

        return json_data($purchases);
    }
    public function purchaseSummaryBySupplierReport(Request $request)
    {
        Gate::authorize('view', 'reports');

        $purchases = Purchase::with('purchaseDetails')->withSum('purchaseDetails', 'qty')->with('supplier')->whereBetween('date', [$request->fromDate, $request->toDate])->when($request->supplier_id, function ($q) {
            return $q->where('supplier_id', request('supplier_id'));
        })->get();

        return json_data($purchases);
    }
    public function viewSalaryEmployeeByMonth(Request $request)
    {


        Gate::authorize('view', 'reports');
        // $salaries = Salary::whereBetween('salary_date', [$request->fromDate, $request->toDate])->with('employee')
        //     ->get()->groupBy('salary_month');

        $monthDoc = Salary::whereBetween('salary_date', [$request->fromDate, $request->toDate])->when($request->month, function ($q) {
            return $q->where('salary_month', request('month'));
        })->select('salary_month')->groupBy('salary_month')->get();

        $salary_doc = $monthDoc->map(function ($val) {
            return [
                'month' => $val->salary_month,
                'details' => Salary::with('employee')->where('salary_month', $val->salary_month)->get()
            ];
        });


        return json_data($salary_doc);
    }


    public function saleReport(Request $request)
    {

        Gate::authorize('view', 'reports');
        $sale = DB::table('orders')->join('users', 'orders.user_id', 'users.id')->select('orders.*', 'users.username')->whereBetween('order_date', [$request->fromDate, $request->toDate])->when($request->user_id, function ($q) {
            return $q->where('orders.user_id', request('user_id'));
        })->get();

        // orWhere('purchases.supplier_id', $request->supplier_id)->



        return json_data($sale);
    }
    public function saleDetailByEmployee(Request $request)
    {

        Gate::authorize('view', 'reports');



        $sales = Order::with('orderDetail')->with('user')->with('table')->whereBetween('order_date', [$request->fromDate, $request->toDate])->where('order_status','paid')->when($request->user_id, function ($q) {
            return $q->where('user_id', request('user_id'));
        })->get();



        return json_data($sales);
    }
    public function saleSummaryByEmployeeReport(Request $request)
    {
        Gate::authorize('view', 'reports');

        $sales = Order::with('orderDetail')->withSum('orderDetail', 'quantity')->with('user')->with('table')->whereBetween('order_date', [$request->fromDate, $request->toDate])->when($request->user_id, function ($q) {
            return $q->where('user_id', request('user_id'));
        })->get();

        return json_data($sales);
    }
    public function chart()
    {
        $purchases = Purchase::query()->join('purchase_details', 'purchases.id', 'purchase_details.purchase_id')
            ->selectRaw("DATE_FORMAT(purchases.created_at,'%Y-%m-%d') as purchase_date, SUM(purchase_details.qty * purchase_details.price) as sum ")->groupBy('purchase_date')->get();

            return json_data($purchases);
    }
}
