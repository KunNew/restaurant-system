<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Http\Resources\RoleCollection;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Gate::authorize('view', 'roles');

        $roles = Role::where([
            ['name', '!=', Null],
            [function ($query) use ($request) {
                if (($s = $request->search)) {
                    $query->orWhere('name', 'LIKE', '%' . $s . '%')->get();
                }
            }]
        ])->paginate(request('perpage', 2));

        return new RoleCollection($roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        Gate::authorize('create', 'roles');
        $role = new Role();
        $role->name = $request->name;
        if ($role->save()) {
            $role->permissions()->attach($request->permission_id);
            return success();
        }
        return server_error();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return json_data([
            'id' => $role->id,
            'name' => $role->name,
            'permission_id' => $role->where('id', $role->id)->with('permissions')->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $this->authorize('edit', 'roles');
        return json_data([
            'data' => [
                'id' => $role->id,
                'name' => $role->name,
                'permission_id' => $role->where('id', $role->id)->with('permissions')->first()
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRoleRequest $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {

        $this->authorize('edit', 'roles');

        $role->name = $request->name;
        if ($role->save()) {
            $role->permissions()->sync($request->permission_id);
            return success();
        } else {
            return server_error();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $this->authorize('delete', 'roles');

        if ($role->delete()) {
            return success();
        } else {
            return server_error();
        }
    }

    public function roles()
    {
        $roles = Role::all();
        return json_data($roles);
    }
}
