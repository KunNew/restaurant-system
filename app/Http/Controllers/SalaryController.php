<?php

namespace App\Http\Controllers;

use App\Models\Salary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class SalaryController extends Controller
{
    public function givenSalaryToEmployee(Request $request)
    {
        Gate::authorize('view', 'pays');

        $salaryDoc = Salary::where('employee_id', $request->employee_id)->where('salary_month', $request->salary_month)->first();
        if ($salaryDoc) {
            return response()->json(['message' => 'Already Paid'], Response::HTTP_BAD_REQUEST);
        } else {
            $salary = new Salary();
            $salary->amount = $request->amount;
            $salary->employee_id = $request->employee_id;
            $salary->salary_date = date('Y/m/d');
            $salary->salary_month = $request->salary_month;
            $salary->salary_year = date('Y');
            if ($salary->save()) return success();
            return server_error();
        }
    }
}
