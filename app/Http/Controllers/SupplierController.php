<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSupplierRequest;
use App\Http\Requests\UpdateSupplierRequest;
use App\Http\Resources\SupplierCollection;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        Gate::authorize('view', 'suppliers');

        $suppliers = Supplier::where([
            ['name', '!=', Null],
            [function ($query) use ($request) {
                if (($s = $request->search)) {
                    $query->orWhere('name', 'LIKE', '%' . $s . '%')->get();
                    $query->orWhere('email', 'LIKE', '%' . $s . '%')->get();
                    $query->orWhere('address', 'LIKE', '%' . $s . '%')->get();
                    $query->orWhere('telephone', 'LIKE', '%' . $s . '%')->get();
                }
            }]
        ])->paginate(request('perpage', 2));

        return new SupplierCollection($suppliers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSupplierRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSupplierRequest $request)
    {
        Gate::authorize('create', 'suppliers');
        $supplier = new Supplier();
        $supplier->name = $request->name;
        $supplier->email = $request->email;
        $supplier->address = $request->address;
        $supplier->company_name = $request->company_name;
        $supplier->telephone = $request->telephone;
        if ($supplier->save()) return success();
        return server_error();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        return json_data($supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        $this->authorize('edit', 'suppliers');
        return json_data([
            'data' => [
                'id' => $supplier->id,
                'name' => $supplier->name,
                'email' => $supplier->email,
                'address' => $supplier->address,
                'telephone' => $supplier->telephone,
                'company_name' => $supplier->company_name,
                'avatar' => null,
            ],
            'additional' => [
                'avatar' => $supplier->avatar,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSupplierRequest  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSupplierRequest $request, Supplier $supplier)
    {
        $this->authorize('edit', 'suppliers');
        $supplier->name = $request->name;
        $supplier->email = $request->email;
        $supplier->address = $request->address;
        $supplier->company_name = $request->company_name;
        $supplier->telephone = $request->telephone;
        if ($supplier->save()) return success();
        return server_error();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        $this->authorize('delete', 'suppliers');
        if ($supplier->delete()) return success();
        return server_error();
    }

    public function supplier()
    {
        $suppliers = Supplier::all();
        return json_data($suppliers);
    }
}
