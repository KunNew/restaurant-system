<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserCollection;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('view', 'users');

        $users = User::paginate(request('perpage', 15));

        return new UserCollection($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        Gate::authorize('create', 'users');
        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->avatar = $request->avatar;
        $user->email_verified_at = now();
        // $user->password = Hash::make('12345678');
        $user->remember_token = Str::random(10);

        if ($user->save()) {
            // $user->sendEmailVerificationNotification();
            return success();
        }

        return server_error();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        return json_data($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('edit', 'users');

        return json_data([
            'data' => [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'username' => $user->username,
                'email' => $user->email,
                'role_id' => $user->role_id,
                'avatar' => null,
            ],
            'additional' => [
                'avatar' => $user->avatar,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('edit', 'users');
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->avatar = $request->avatar;
        if ($user->save()) return success();

        return server_error();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        if ($user->delete()) return success();

        return server_error();
    }

    public function onlyTrash()
    {
        $users = User::onlyTrashed()->paginate(request('perpage', 15));

        return new UserCollection($users);
    }



    public function restoreUser($id)
    {

        if (User::onlyTrashed()->where('id', $id)->restore()) {
            return success();
        }
        return server_error();
    }

    public function forceDelete($id)
    {
        $this->authorize('delete', 'users');
        if (User::onlyTrashed()->where('id', $id)->forceDelete()) return success();
        return server_error();
    }

    public function user()
    {
        $user = User::all();
        return json_data($user);
    }
}
