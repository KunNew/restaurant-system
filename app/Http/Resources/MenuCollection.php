<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MenuCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function with($request)
    {
        return [
            'headers' => [
                ['text' => 'No', 'sortable' => false, 'value' => 'no'],
                ['text' => 'Name', 'sortable' => false, 'value' => 'name'],
                ['text' => 'Cost', 'sortable' => false, 'value' => 'cost'],
                ['text' => 'Price', 'sortable' => false, 'value' => 'price'],
                ['text' => 'Description', 'sortable' => false, 'value' => 'description'],
                ['text' => 'Actions', 'sortable' => false, 'value' => 'actions'],
            ]
        ];
    }
}
