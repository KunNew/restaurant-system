<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PurchaseCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
    public function with($request)
    {
        return [
            'headers' => [
                ['text' => 'No', 'sortable' => false, 'value' => 'no'],
                ['text' => 'Date', 'sortable' => false, 'value' => 'date'],
                ['text' => 'Supplier', 'sortable' => false, 'value' => 'supplier.name'],
                ['text' => 'Total', 'sortable' => false, 'value' => 'total'],
                // ['text' => 'Description', 'sortable' => false, 'value' => 'description'],
                // ['text' => 'Actions', 'sortable' => false, 'value' => 'actions'],
            ]
        ];
    }
}
