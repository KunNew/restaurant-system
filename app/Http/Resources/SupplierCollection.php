<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SupplierCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function with($request)
    {
        return [
            'headers' => [
                ['text' => 'No', 'sortable' => false, 'value' => 'no'],
                ['text' => 'Name', 'sortable' => false, 'value' => 'name'],
                ['text' => 'Email', 'sortable' => false, 'value' => 'email'],
                ['text' => 'Address', 'sortable' => false, 'value' => 'address'],
                ['text' => 'Telephone', 'sortable' => false, 'value' => 'telephone'],
                ['text' => 'Company', 'sortable' => false, 'value' => 'company_name'],
                ['text' => 'Actions', 'sortable' => false, 'value' => 'actions'],
            ]
        ];
    }
}
