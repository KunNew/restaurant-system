<?php

namespace App\Http\Resources\landing;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'photo' => $this->avatar,
            'category_id' => $this->category_id,
            'category' => new CategoryResource($this->whenLoaded('category')),
            'qty' => $this->qty,
            'cost' => $this->cost,
            'price' => $this->price,
            'unit' => $this->unit,
            'status' => $this->status,
            'description' => $this->description



        ];
    }
}
