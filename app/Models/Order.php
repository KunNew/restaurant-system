<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'table_id',
        'user_id',
        'order_date',
        'total_price',
        'total_recieved',
        'change',
        'payment_type',
        'order_status'
    ];


    public function table ()
    {
        return $this->belongsTo(Table::class);
    }
    public function orderDetail ()
    {
        return $this->hasMany(OrderDetail::class);
    }
    public function user ()
    {
        return $this->belongsTo(User::class);
    }
}
