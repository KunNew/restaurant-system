<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'menu_id',
        'menu_name',
        'menu_price',
        'unit',
        'status',
        'quantity',
        'amount'

    ];

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
}
