<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;

    protected $fillable = [
        'supplier_id',
        'user_id',
        'date',
        'total',

    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class)->select('id','name');
    }
    public function purchaseDetails() {
        return $this->hasMany(PurchaseDetail::class);
    }
}
