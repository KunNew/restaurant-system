<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        // JsonResource::withoutWrapping();

        Gate::define('view', function (User $user, $model) {
            return $user->hasAccess("view_{$model}");
        });

        Gate::define('create', fn (User $user, $model) => $user->hasAccess("create_{$model}"));

        Gate::define('edit', fn (User $user, $model) => $user->hasAccess("edit_{$model}"));


        Gate::define('delete', fn (User $user, $model) => $user->hasAccess("delete_{$model}"));
    }
}
