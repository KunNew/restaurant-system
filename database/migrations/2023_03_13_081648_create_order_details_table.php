<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->integer('menu_id');
            $table->string('menu_name');
            $table->decimal('menu_price');
            $table->tinyInteger('status')->default(0);
            $table->string('unit');
            $table->integer('quantity');
            $table->decimal('amount');


            // $table->string('status')->default('noConfirm'); // confirm or noconfirm
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
};
