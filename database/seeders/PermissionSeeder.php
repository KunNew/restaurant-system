<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            //user
            ['name' => 'view_users'],
            ['name' => 'create_users'],
            ['name' => 'edit_users'],
            ['name' => 'delete_users'],
            ['name' => 'trash_users'],
            //role
            ['name' => 'view_roles'],
            ['name' => 'create_roles'],
            ['name' => 'edit_roles'],
            ['name' => 'delete_roles'],
            //employee
            ['name' => 'view_employees'],
            ['name' => 'create_employees'],
            ['name' => 'edit_employees'],
            ['name' => 'delete_employees'],
            //pay
            ['name' => 'view_pays'],
            //supplier
            ['name' => 'view_suppliers'],
            ['name' => 'create_suppliers'],
            ['name' => 'edit_suppliers'],
            ['name' => 'delete_suppliers'],
            //category
            ['name' => 'view_categories'],
            ['name' => 'create_categories'],
            ['name' => 'edit_categories'],
            ['name' => 'delete_categories'],
            //table
            ['name' => 'view_tables'],
            ['name' => 'create_tables'],
            ['name' => 'edit_tables'],
            ['name' => 'delete_tables'],
            //menu
            ['name' => 'view_menus'],
            ['name' => 'create_menus'],
            ['name' => 'edit_menus'],
            ['name' => 'delete_menus'],
            //purchases
            ['name' => 'create_purchases'],
            ['name' => 'view_purchases'],
            //cashiers
            ['name' => 'view_cashiers'],
            ['name' => 'pay_cashiers'],
            //reports
            ['name' => 'view_reports'],
            // ['name' => 'view_products'],
            // ['name' => 'edit_products'],
            // ['name' => 'view_orders'],
            // ['name' => 'edit_orders'],
        ]);
    }
}
