 import store from "../store";
 class Acl {
    constructor(user) {
        this.user = user
    }

    roleName () {

        return this.user.role.name;
    }
    isAdmin () {
        return this.roleName() === 'Admin'
    }
    permissions () {
        return this.user.permissions;
    }
    can (permissionName) {

        return store.state.auth.user.permissions.includes(permissionName)
    }
}

export default Acl;
