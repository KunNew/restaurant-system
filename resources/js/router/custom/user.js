export default [
    {
        path: "/user",
        name: "user.index",
        component: () => import("./../../views/user/Index.vue"),
    },
    {
        path: "/user/create",
        name: "user.create",
        component: () => import("./../../views/user/Create.vue"),
    },
    {
        path: "/user/:user/edit",
        name: "user.edit",
        component: () => import("./../../views/user/Edit.vue"),
    },
    {
        path: "/user/:user/show",
        name: "user.show",
        component: () => import("./../../views/user/Show.vue"),
    },
    {
        path: "/user-trash",
        name: "user.trash",
        component: () => import("./../../views/user/Trash.vue"),
    },

    //role
    {
        path: "/role",
        name: "role.index",
        component: () => import("./../../views/role/Index.vue"),
    },
    {
        path: "/role/create",
        name: "role.create",
        component: () => import("./../../views/role/Create.vue"),
    },
    {
        path: "/role/:role/edit",
        name: "role.edit",
        component: () => import("./../../views/role/Edit.vue"),
    },
    {
        path: "/role/:role/show",
        name: "role.show",
        component: () => import("./../../views/role/Show.vue"),
    },

    //supplier
    {
        path: "/supplier",
        name: "supplier.index",
        component: () => import("./../../views/supplier/Index.vue"),
    },
    {
        path: "/supplier/create",
        name: "supplier.create",
        component: () => import("./../../views/supplier/Create.vue"),
    },
    {
        path: "/supplier/:supplier/edit",
        name: "supplier.edit",
        component: () => import("./../../views/supplier/Edit.vue"),
    },
    {
        path: "/supplier/:supplier/show",
        name: "supplier.show",
        component: () => import("./../../views/supplier/Show.vue"),
    },

    // employee
    {
        path: "/employee",
        name: "employee.index",
        component: () => import("./../../views/employee/Index.vue"),
    },
    {
        path: "/employee/create",
        name: "employee.create",
        component: () => import("./../../views/employee/Create.vue"),
    },
    {
        path: "/employee/:employee/edit",
        name: "employee.edit",
        component: () => import("./../../views/employee/Edit.vue"),
    },
    {
        path: "/employee/:employee/show",
        name: "employee.show",
        component: () => import("./../../views/employee/Show.vue"),
    },

    {
        path: "/givenSalary/employee",
        name: "employee.givenSalary",
        component: () => import("./../../views/employee/GivenSalary.vue"),
    },

    //category
    {
        path: "/category",
        name: "category.index",
        component: () => import("./../../views/category/Index.vue"),
    },
    {
        path: "/category/create",
        name: "category.create",
        component: () => import("./../../views/category/Create.vue"),
    },
    {
        path: "/category/:category/edit",
        name: "category.edit",
        component: () => import("./../../views/category/Edit.vue"),
    },
    {
        path: "/category/:category/show",
        name: "category.show",
        component: () => import("./../../views/category/Show.vue"),
    },
    // table
    {
        path: "/table",
        name: "table.index",
        component: () => import("./../../views/table/Index.vue"),
    },
    {
        path: "/table/create",
        name: "table.create",
        component: () => import("./../../views/table/Create.vue"),
    },
    {
        path: "/table/:table/edit",
        name: "table.edit",
        component: () => import("./../../views/table/Edit.vue"),
    },
    {
        path: "/table/:table/show",
        name: "table.show",
        component: () => import("./../../views/table/Show.vue"),
    },

    //menu
    {
        path: "/menu",
        name: "menu.index",
        component: () => import("./../../views/menu/Index.vue"),
    },
    {
        path: "/menu/create",
        name: "menu.create",
        component: () => import("./../../views/menu/Create.vue"),
    },
    {
        path: "/menu/:menu/edit",
        name: "menu.edit",
        component: () => import("./../../views/menu/Edit.vue"),
    },
    {
        path: "/menu/:menu/show",
        name: "menu.show",
        component: () => import("./../../views/menu/Show.vue"),
    },

    //purchase

    {
        path: "/purchase",
        name: "purchase.index",
        component: () => import("./../../views/purchase/Index.vue"),
    },
    {
        path: "/purchase/show",
        name: "purchase.show",
        component: () => import("./../../views/purchase/Show.vue"),
    },
    {
        path: "/cashier",
        name: "cashier.index",
        component: () => import("./../../views/cashier/Index.vue"),
    },

    //report
    {
        path: "/purchase-report",
        name: "purchase.report",
        component: () =>
            import("./../../views/report/purchase/PurchaseReport.vue"),
    },

    {
        path: "/purchaseDetailBySupplier-report",
        name: "purchaseDetailBySupplier.report",
        component: () =>
            import(
                "../../views/report/purchase/PurchaseDetailBySupplierReport.vue"
            ),
    },

    {
        path: "/purchaseSummaryBySupplier-report",
        name: "purchaseSummaryBySupplier.report",
        component: () =>
            import(
                "../../views/report/purchase/PurchaseSummaryBySupplierReport.vue"
            ),
    },
    {
        path: "/saleDetailByEmployee-report",
        name: "saleDetailByEmployee.report",
        component: () =>
            import("../../views/report/sale/SaleDetailByEmployeeReport.vue"),
    },
    {
        path: "/saleSummaryBySupplier-report",
        name: "saleSummaryBySupplier.report",
        component: () =>
            import(
                "../../views/report/sale/SaleSummaryByEmployeeReport.vue"
            ),
    },

    {
        path: "/viewSalaryEmployeeByMonth-report",
        name: "viewSalaryEmployeeByMonth.report",
        component: () =>
            import(
                "../../views/report/employee/ViewSalaryEmployeeByMonthReport.vue"
            ),
    },

    {
        path: "/sale-report",
        name: "sale.report",
        component: () => import("./../../views/report/sale/SaleReport.vue"),
    },

    // setting
    {
        path: "/setting",
        name: "setting",
        component: () => import("./../../views/Setting.vue"),
    },

    //receipt
    {
        path: "/receipt",
        name: "receipt.report",
        component: () => import("./../../views/receipt/Receipt.vue"),
    },
];
