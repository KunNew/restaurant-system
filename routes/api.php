<?php

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SalaryController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\TableController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [LoginController::class, 'login']);
Route::post('refresh', [LoginController::class, 'refresh']);

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return response()->json(['message' => 'verify success']);
})->middleware(['auth:api', 'signed'])->name('verification.verify');

Route::get('/forgot-password', function () {
    return view('auth.forgot-password');
})->middleware('guest')->name('password.request');

Route::post('/forgot-password', function (Request $request) {
    $request->validate(['email' => 'required|email']);

    $status = Password::sendResetLink(
        $request->only('email')
    );

    if ($status === Password::RESET_LINK_SENT) {
        return success('Your password reset link has been sent to your email.');
    }

    return server_error(__($status));
})->middleware('guest')->name('password.email');

Route::post('/reset-password', function (Request $request) {
    $request->validate([
        'token' => 'required',
        'email' => 'required|email',
        'password' => 'required|min:8|confirmed',
    ]);

    $status = Password::reset(
        $request->only('email', 'password', 'password_confirmation', 'token'),
        function ($user, $password) {
            $user->forceFill([
                'password' => Hash::make($password)
            ])->setRememberToken(Str::random(60));

            $user->save();

            event(new PasswordReset($user));
        }
    );

    if ($status === Password::PASSWORD_RESET) {
        return success('Password has been reset');
    }

    return server_error(__($status));
})->middleware('guest')->name('password.update');

Route::middleware(['auth:api'])->get('/auth/user', [AuthController::class, 'user']);
Route::group([
    'middleware' => ['auth:api', 'verified'],
], function () {


    Route::put('setting/change_profile/{id}', [AuthController::class, 'changeProfile']);
    Route::post('setting/change_password', [AuthController::class, 'changePassword']);


    Route::get('user-onlyTrash', [UserController::class, 'onlyTrash']);
    Route::get('user-restore/{id}', [UserController::class, 'restoreUser']);
    Route::get('user-forceDelete/{id}', [UserController::class, 'forceDelete']);
    Route::get('all-user', [UserController::class, 'user']);
    Route::resource('user', UserController::class);

    Route::get('all-role', [RoleController::class, 'roles']);
    Route::resource('role', RoleController::class);

    Route::get('/permission', [PermissionController::class, 'index']);

    Route::get('all-category', [CategoryController::class, 'category']);
    Route::get('category/export', [CategoryController::class, 'export']);

    Route::resource('category', CategoryController::class);

    Route::get('all-table', [TableController::class, 'getTables']);
    Route::resource('table', TableController::class);

    Route::get('all-menu', [MenuController::class, 'menu']);
    Route::resource('menu', MenuController::class);
    Route::get('all-supplier', [SupplierController::class, 'supplier']);
    Route::resource('supplier', SupplierController::class);


    Route::get('all-employee', [EmployeeController::class, 'employee']);
    Route::resource('employee', EmployeeController::class);
    Route::post('given-salary', [SalaryController::class, 'givenSalaryToEmployee']);

    Route::get('purchase', [PurchaseController::class, 'index']);
    Route::post('purchase', [PurchaseController::class, 'addPurchase']);

    //report
    Route::post('purchase-report', [ReportController::class, 'purchaseReport']);
    Route::post('purchaseDetailBySupplier-report', [ReportController::class, 'purchaseDetailBySupplier']);
    Route::post('purchaseSummaryBySupplier-report', [ReportController::class, 'purchaseSummaryBySupplierReport']);
    Route::post('viewSalaryEmployeeByMonth-report', [ReportController::class, 'viewSalaryEmployeeByMonth']);

    Route::post('sale-report', [ReportController::class, 'saleReport']);
    Route::post('saleDetailByEmployee-report', [ReportController::class, 'saleDetailByEmployee']);
    Route::post('saleSummaryByEmployee-report', [ReportController::class, 'saleSummaryByEmployeeReport']);
    Route::get('chart',[ReportController::class,'chart']);


    //landing page


    // order
    Route::post('landing-confirmOrder',[OrderController::class,'confirmOrder']);
    Route::get('landing-myOrder',[OrderController::class,'getMyOrders']);
    Route::delete('landing-cancelOrder/{id}',[OrderController::class,'cancelOrder']);

    Route::get('order-findMenuByTableId/{id}',[OrderController::class,'getMenuByTableId']);
    Route::post('pay-order',[OrderController::class,'payOrder']);


});

//landing page
Route::get('landing-category',[LandingController::class,'getCategories']);
Route::get('landing-table',[LandingController::class,'getTables']);
Route::get('landing-menu',[LandingController::class,'getMenus']);
Route::get('landing-menu/{id}',[LandingController::class,'getMenu']);
Route::get('landing-filterMenu',[LandingController::class,'getFilterMenus']);




